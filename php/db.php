<pre>
<?php
function get_sql_connection() {
    $xml = file_get_contents('mysql_conf.xml');
    $login = new SimpleXMLElement($xml);
    $mysqli = new mysqli($login->host, $login->username, $login->passwd, $login->dbname)
                or die($mysqli->errno . ': ' . $mysqli->error);
    return $mysqli;
}
?>
</pre>
