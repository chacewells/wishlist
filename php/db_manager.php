<?php
class DBManager {
    public $mysql;
    public function __constructor($mysql) {
        $this->mysql = $mysql;
    }

    public function create_surname($surname) {
        // check whether the surname already exists
        // add surname if not
    }

    public function read_surnames() {
        // build query
        // execute query
        // capture query results in php array
        // return the array
    }

    public function read_surname_by_id($id) {
        // build query
        // execute query
        // capture result
        // return surname as string
    }

    public function read_surname_id($surname) {
        // build query
        // execute
        // capture result
        // return id as int
    }

    public function update_surname($id, $new_surname) {
        // build update statement
        // execute
        // return true if successful, false otherwise
    }

    public function destroy_surname_by_id($id) {
        // build delete statement
        // execute
        // return true if successful, false otherwise
    }

    public function destroy_surname_by_surname($surname) {
        // build delete statement
        // execute
        // return true if successful, false otherwise
    }

    public function create_user($first_name, $last_name, $surname) {
        // check whether user already exists
        // add user if not
    }

    public function read_users() {
        // build query
        // execute query
        // capture results in associative php array
        // return the array
    }

    public function read_user($first_name, $last_name) {
        // build query
        // execute query
        // capture result in associative php array
        // return the array
    }

    public function update_user($id, $new_first, $new_last, $new_surname) {
        // build update statement
        // execute
        // return true if successful, false otherwise
    }

    public function destroy_user($first_name, $last_name) {
        // build delete statement
        // execute
        // return true if successful, false otherwise
    }

    public function create_item($title, $description, $url, $image, $user_id) {
        // check whether item already exists
        // add item if not
    }

    public function read_items() {
        // build query
        // execute query
        // capture result in associative php array
        // return the array
    }

    public function read_item($title) {
        // build query
        // execute query
        // capture result in associative php array
        // return the array
    }

    public function update_item($id, $new_title, $new_description, $new_url, $new_image, $new_user_id) {
        // build update statement
        // execute
        // return true if successful, false otherwise
    }

}
?>
